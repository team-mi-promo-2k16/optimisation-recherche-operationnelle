# Introduction

Prérequis :

* Quicksort
* Dijkstra
* Kruskal
* Ford-Fulkerson


Se souvenir de ses cours de graphes et cours d'optimisation linéaire

Algo du simplexe: exponentiel en le nombre de dimensions (en théorie), polynomial
le plus souvent en pratique.


# Optimisation des paraboles (gradient)

## Minimiser `f := x -> x²` sur [-5; 5]

On choisit $x0 \in [-5; 5]$, on utilise la dérivée pour savoir dans quelle direction
aller, en fonction du signe de celle-ci en x0.

En itérant, on se retrouve avec un extremum (s'il existe, sinon on se retrouve dans
une boucle infinie), local ou non (le plus souvent local, malheureusement)

$$x_{k + 1} = x_k - \rho f'(x_k)$$


## Analogie avec l'algorithmique

Problème analogue des méthodes gloutonnes en algo :
une suite de décisions localement optimales ne garantit pas de trouver l'optimum
global.


## `f := (x, y) -> x² - y²`

Si f est dérivable, les points candidats pour être optimaux sont ceux où f'(x) = 0.

## Théorème admis (Abel)

Il n'existe pas de résolution générale par radicaux (+, -, *, /, $\sqrt{}$) pour les
polynômes de degré $\geq$ 5.


# Méthode de Newton-Raphson

`f := x -> x² - 2`

On utilise la méthode de Newton-Raphson pour trouver le zéro
de la fonction f, connaissant sa dérivée ff. On part d'un point
x0, et on trouve x tel que f'(x0) * x + f(x0) = 0 (équation de la
tangente). Ensuite, on itère jusqu'à trouver un point fixe.
On utilise la méthode de Newton-Raphson pour trouver le zéro
de la fonction f, connaissant sa dérivée ff. On part d'un point
x0, et on trouve x tel que f'(x0) * x + f(x0) = 0 (équation de la
tangente). Ensuite, on itère jusqu'à trouver un point fixe.


# Transformation à aire constante

Pour calculer sqrt(2), on part d'un rectangle d'aire égale à 2,
et on essaie de transformer ce rectangle en un carré de même aire,
ainsi, la longueur d'un côté de ce carré sera égale à sqrt(2).

# Méthodes à pas variable

Ici, on reprend la première méthode, mais on ne fixe plus le $\rho$.

## Méthode d'Armijo

Idée : faire des pas plus grands, pour descendre plus vite.

$$x_{k+1} = \min \{x_k - j\rho f'(x_k)\},  j \in [32,16,..1]$$

On peut arrêter l'algorithme lorsque la dérivée est suffisement plate,
c'est à dire que $f'(x_k) < \epsilon$, avec $\epsilon$ fixé.

# Références

* Algorithms Part. I (Sedgewick, Coursera)
* Clever Algorithms (Browlee)

ε = 1e-10

def Newton(f, ff, x0):
    '''
    On utilise la méthode de Newton-Raphson pour trouver le zéro
    de la fonction f, connaissant sa dérivée ff. On part d'un point
    x0, et on trouve x tel que f'(x0) * x + f(x0) = 0 (équation de la
    tangente). Ensuite, on itère jusqu'à trouver un point fixe.
    '''
    x1 = x0 - f(x0) / ff(x0)
    
    if abs(x1 - x0) < ε:
        return x1

    return Newton(f, ff, x1)

if __name__ == '__main__':
    print(Newton(lambda x: x ** 2 - 2, lambda x: 2 * x, 2.))


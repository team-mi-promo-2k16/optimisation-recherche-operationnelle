ε = 1e-10

def rrange():
    '''
    On génère la suite de nombres 1, 2, 4, 8, 16, 32
    '''
    return (x ** 2 for x in range(1, 6))

def Armijo(f, ff, x0, ρ):
    '''
    x_{k + 1} = min{x_k - jρ*f'(x_k), j \in [32,16,...,1]}
    '''
    x1 = min(x0 - j * ρ * ff(x0) for j in rrange())

    if abs(x1 - x0) < ε:
        return x1

    return Armijo(f, ff, x1, ρ)

if __name__ == '__main__':
    print(Armijo(lambda x: x ** 2, lambda x: 2 * x, 42., 0.1))


ε = 1e-15

def Rectangle(a, b):
    x = (a + b) / 2.
    y = (a * b) / x

    if abs(x - y) < ε:
        return x

    return Rectangle(x, y)

def Sqrt2():
    '''
    Pour calculer sqrt(2), on part d'un rectangle d'aire égale à 2,
    et on essaie de transformer ce rectangle en un carré de même aire,
    ainsi, la longueur d'un côté de ce carré sera égale à sqrt(2)
    '''
    return Rectangle(1., 2.)

if __name__ == '__main__':
    print(Sqrt2())


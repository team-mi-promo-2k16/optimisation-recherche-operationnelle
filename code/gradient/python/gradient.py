import operator
import math

ε = 1e-20

def dist0(a, b):
    return abs(a[0] - b[0])

def f1(a):
    x, y = a
    return x ** 2 + y ** 2

def ff1(a):
    x, y = a
    return 2 * x, 2 * y

def dist1(a, b):
    x0, y0 = a
    x1, y1 = b

    return (x0 - x1) ** 2 + (y0 - y1) ** 2

def f2(a):
    x = a[0]
    return x ** 2 + math.sin(x)

def ff2(a):
    x = a[0]
    return (2 * x + math.cos(x),)

#########################

def Difference(u, v):
    return tuple(map(operator.sub, u, v))

def Multiply(λ, t):
    return tuple(λ * x for x in t)

def Optimize(f, ff, x0, ρ, dist):
    x1 = Difference(x0, Multiply(ρ, ff(x0)))
    
    if dist(x0, x1) < ε:
        return x0

    return Optimize(f, ff, x1, ρ, dist)

if __name__ == '__main__':
    print(Optimize(lambda x: x ** 2, lambda x: 2 * x, (5.,), 0.1, dist0))

    print(Optimize(f1, ff1, (5., 5.), 0.1, dist1))

    print(Optimize(f2, ff2, (5.,), 0.1, dist0))


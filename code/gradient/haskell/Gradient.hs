module Gradient where


optim1D :: (Double -> Double) -- f 
        -> (Double -> Double) -- f' 
        -> Double -- x
        -> Double
optim1D f f' x 
    | abs (x1 - x) < ε = x
    | otherwise = optim1D f f' x1
    
    where ρ = 0.1
          ε = 10 ** (-10)

          x1 = x - ρ * f' x 

optim2D :: ((Double, Double) -> Double) -- f
        -> ((Double, Double) -> (Double, Double)) -- ∇f
        -> Double -- x
        -> Double -- y
        -> (Double, Double)

optim2D f f' x y
    | (x' - x) ** 2 + (y' - y) ** 2 < ε = (x, y)
    | otherwise = optim2D f f' x' y'

      where ρ = 0.1
            ε = 10 ** (-10)

            (x_, y_) = f' (x, y)
            x' = x - ρ * x_
            y' = y - ρ * y_

main :: IO ()
main = do
  print $ optim1D (\x -> x ** 2) (\x -> 2 * x) 2.0
  print $ optim2D (\(x, y) -> x ** 2 + y ** 2) (\(x, y) -> (2 * x, 2 * y)) 1.0 1.0
  print $ optim1D (\x -> x ** 2 + sin x) (\x -> 2 * x + cos x) 5.0
  print $ optim1D (\x -> x ** 2 + sin x) (\x -> 2 * x + cos x) (-500.0)